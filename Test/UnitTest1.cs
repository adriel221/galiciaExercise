using Galicia;
using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class UnitTest1
    {

        [Fact]
        public void TestPrintEmptyList()
        {
            Printer _printer = new Printer(1);
            Assert.Equal("Lista vac�a de formas!",
                _printer.Print(new List<Figure>()));
        }

        [Fact]
        public void TestPrintEmptyListEnglishLanguage()
        {
            Printer _printer = new Printer(2);
            Assert.Equal("Empty list of shapes!",
                _printer.Print(new List<Figure>()));
        }

        [Fact]
        public void TestPrintSquare()
        {

            var squares = new List<Figure> { Creator.CreatorFigure(1, 5) };
            Printer _printer = new Printer(1);
            var result = _printer.Print(squares);

            Assert.Equal("Reporte de Formas 1 Cuadrado | Area 25 | Perimetro 20 TOTAL: 1 formas Perimetro 20 Area 25", result);

        }
        
        [Fact]
        public void TestPrintSquaresEnglishLanguage()
        {
            
            var squares = new List<Figure>
            {
                Creator.CreatorFigure(Figure.Square,5),
                Creator.CreatorFigure(Figure.Square,2),
                Creator.CreatorFigure(Figure.Square,3)
            };

            Printer _printer = new Printer(2);
            var result = _printer.Print(squares);

            Assert.Equal("Shapes report 3 Squares | Area 35 | Perimeter 36 TOTAL: 3 shapes Perimeter 36 Area 35", result);
            
        }

        [Fact]
        public void TestPrintMixFigureTypesEnglishLanguage()
        {
            var figures = new List<Figure>
            {
                Creator.CreatorFigure(Figure.Square,5),
                Creator.CreatorFigure(Figure.Circle,3),
                Creator.CreatorFigure(Figure.Triangle, 4),
                Creator.CreatorFigure(Figure.Square, 2),
                Creator.CreatorFigure(Figure.Triangle, 9),
                Creator.CreatorFigure(Figure.Circle, 2.75m),
                Creator.CreatorFigure(Figure.Triangle, 4.2m)
            };
            Printer _printer = new Printer(2);

            var response = _printer.Print(figures);

            Assert.Equal(
                "Shapes report 2 Squares | Area 29 | Perimeter 28 2 Circles | Area 13.01 | Perimeter 18.06 3 Triangles | Area 49.64 |" +
                " Perimeter 51.6 TOTAL: 7 shapes Perimeter 97.66 Area 91.65",
                response);
        }

        [Fact]
        public void TestPrintMixTypesEnglishLanguage()
        {
            var figures = new List<Figure>
            {
                Creator.CreatorFigure(Figure.Square, 5),
                Creator.CreatorFigure(Figure.Circle, 3),
                Creator.CreatorFigure(Figure.Triangle, 4),
                Creator.CreatorFigure(Figure.Square, 2),
                Creator.CreatorFigure(Figure.Triangle, 9),
                Creator.CreatorFigure(Figure.Circle, 2.75m),
                Creator.CreatorFigure(Figure.Triangle, 4.2m)
            };
            Printer _printer = new Printer(1);
            var response = _printer.Print(figures);
            
            Assert.Equal(
                "Reporte de Formas 2 Cuadrados | Area 29 | Perimetro 28 2 C�rculos | Area 13.01 | Perimetro 18.06 3 Tri�ngulos | Area 49.64 |" +
                " Perimetro 51.6 TOTAL: 7 formas Perimetro 97.66 Area 91.65",
                response);
        }
        
    }
}
