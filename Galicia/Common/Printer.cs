﻿
using Galicia.Languajes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Galicia
{
    public class Printer 
    {
        public const int Spanish = 1;
        public const int English = 2;
        private List<Recuento> Totales = new List<Recuento>();
        private Language _language;
        
        public void Update(int type,decimal perimeter,decimal area)
        {

        }
        public Printer(int language)
        {
            switch (language)
            {
                case Spanish:
                    _language = new Spanish();
                    break;
                case English:
                    _language = new English();
                    break;
            }
        }

        public string Print(IList<Figure> figures)
        {
            var sb = new StringBuilder();
            System.IFormatProvider cultureUS = new System.Globalization.CultureInfo("en-US");
            if (!figures.Any())
            {
                sb.Append(_language.emptyList);
            }
            else
            {
                sb.Append(_language.reportName + " ");


                for (var i = 0; i < figures.Count; i++)
                {
                    var _item = Totales.FirstOrDefault(o => o.type == figures[i].type);

                    if (_item == null)
                    {
                        Totales.Add(new Recuento(figures[i].type, figures[i].CalculateArea(), figures[i].CalculatePerimeter()));
                    }
                    else
                    {
                        _item.quantity++;
                        _item.perimeter += figures[i].CalculatePerimeter();
                        _item.area += figures[i].CalculateArea();
                    }
                }

                int cantidadFormas = 0;
                decimal totalPerimetro = 0;
                decimal totalArea = 0;
                foreach (Recuento item in Totales)
                {
                    if (item != null)
                    {
                        sb.Append(item.quantity + " "+_language.Description(item.type, item.quantity) + " | ");
                        sb.Append(_language.area +" "+ item.area.ToString("#.##", cultureUS) + " | ");
                        sb.Append(_language.perimeter + " " + item.perimeter.ToString("#.##",cultureUS) +" ");

                        cantidadFormas+=item.quantity;
                        totalPerimetro += item.perimeter;
                        totalArea += item.area;
                    }
                }
                sb.Append("TOTAL: ");
                sb.Append(cantidadFormas.ToString() + " " + _language.shapes + " ");
                sb.Append(_language.perimeter + " " + totalPerimetro.ToString("#.##", cultureUS) +" ");
                sb.Append(_language.area + " " + totalArea.ToString("#.##", cultureUS));
            }
            return sb.ToString();
        }
    }
}
