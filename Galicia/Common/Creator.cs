﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia
{
    public static class Creator
    {
        public const int Square = 1;
        public const int Triangle = 2;
        public const int Circle = 3;

        public static Figure CreatorFigure(int tipo,decimal lengh)
        {
            switch (tipo)
            {
                case Circle:
                    return new Circle(3,lengh);
                case Triangle:
                    return new Triangle(2,lengh);
                case Square:
                    return new Square(1,lengh);
                default:
                    return null;
            }
        }
    }
}
