﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia
{
    public class Recuento
    {
        public int type { get; set; }
        public decimal area { get; set; }
        public decimal perimeter { get; set; }
        public int quantity { get; set; }

        public Recuento(int _type,decimal _area,decimal _perimeter)
        {
            this.type = _type;
            this.area = _area;
            this.perimeter = _perimeter;
            this.quantity = 1;
        }
    }
}
