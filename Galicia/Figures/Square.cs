﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia
{
    class Square : Figure
    {
        public Square(int type,decimal lengh) : base(type,lengh){}
        public override decimal CalculateArea()
        {
            return this.side * this.side;
        }
        public override decimal CalculatePerimeter()
        {
            return this.side * 4;
        }
    }
}
