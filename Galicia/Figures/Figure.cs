﻿/*
 * Realizar refactor respetando principios de programación orientada a objetos. 
 * ¿Qué sucede si es necesario soportar un nuevo idioma para los reportes o agregar más formas geométricas?
 *
 * Cualquier cambio es permitido tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Pentágono/Rectangulo, agregar otro idioma a reporting.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galicia
{
    public abstract class Figure
    {
        public const int Square = 1;
        public const int Triangle = 2;
        public const int Circle = 3;

        public const int Spanish = 1;
        public const int English = 2;

        public decimal side { get; set; }
        public int type { get; set; }
        
        public Figure(int _type=0,decimal _lengh=0)
        {
            if (_type != 0 && _lengh != 0)
            {
                type = _type;
                side = _lengh;
            }
        }
        public abstract decimal CalculateArea();
        public abstract decimal CalculatePerimeter(); 
    }
}