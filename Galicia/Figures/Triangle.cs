﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia
{
    class Triangle : Figure
    {
        public Triangle(int type,decimal lengh) : base(type,lengh){}

        public override decimal CalculateArea()
        {
            return ((decimal)Math.Sqrt(3) / 4) * this.side * this.side;
        }

        public override decimal CalculatePerimeter()
        {
            return this.side * 3;
        }
    }
}
