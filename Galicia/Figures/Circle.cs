﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia
{
    public class Circle : Figure
    {
        
        public Circle(int type,decimal lengh):base(type,lengh){ }
        public override decimal CalculateArea()
        {
            return (decimal)Math.PI * (this.side / 2) * (this.side / 2);
        }

        public override decimal CalculatePerimeter()
        {
            return this.side * (decimal)Math.PI;
        }
    }
}
