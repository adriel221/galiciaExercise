﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Galicia.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FiguresController : Controller
    {
        
        [HttpPost]
        public ActionResult PrintFigures([FromBody] FigureParameterDto _parameters)
        {
            List<Figure> _list = new List<Figure>();
            foreach (var item in _parameters.figures)
            {
                Figure _figure = Creator.CreatorFigure(item.type, item.side);
                _list.Add(_figure);
            }

            Printer _context = new Printer(_parameters.language);
            return Ok(_context.Print(_list));
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok("Aplicación para calcular areas y perimetros");
        }
    }
}
