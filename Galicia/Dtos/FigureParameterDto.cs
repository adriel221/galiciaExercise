﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Galicia
{
    public class FigureParameterDto
    {
        public int language { get; set; }
        public List<FigureParameter> figures { get; set; }
    }
}
