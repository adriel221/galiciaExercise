﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia.Languajes
{
    public class English : Language
    {
        public override string perimeter { get { return "Perimeter"; } }
        public override string emptyList { get { return "Empty list of shapes!"; } }
        public override string area { get { return "Area"; } }
        public override string reportName { get { return "Shapes report"; } }
        public override string shapes { get { return "shapes"; } }
        public override string total { get { return "Total"; } }
        
        public override string Description(int type, int count)
        {
            if (count>1)
            {
                switch(type)
                {
                    case Circle:
                        return "Circles";
                    case Triangle:
                        return "Triangles";
                    case Square:
                        return "Squares";
                    default:
                        return string.Empty;
                }
            }
            else
            {
                switch (type)
                {
                    case Circle:
                        return "Circle";
                    case Triangle:
                        return "Triangle";
                    case Square:
                        return "Square";
                    default:
                        return string.Empty;
                }
            }
        }
    }
}
