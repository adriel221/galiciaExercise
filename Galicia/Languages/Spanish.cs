﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia.Languajes
{
    public class Spanish : Language
    {
        public override string perimeter { get { return "Perimetro"; } }
        public override string emptyList { get { return "Lista vacía de formas!"; } }
        public override string area { get { return "Area"; } }
        public override string reportName { get { return "Reporte de Formas"; } }
        public override string shapes {get{ return "formas"; } }
        public override string total { get { return "Total"; } }

        public override string Description(int type, int count)
        {
            if (count > 1)
            {
                switch (type)
                {
                    case Circle:
                        return "Círculos";
                    case Triangle:
                        return "Triángulos";
                    case Square:
                        return "Cuadrados";
                    default:
                        return string.Empty;
                }
            }
            else
            {
                switch (type)
                {
                    case Circle:
                        return "Círculo";
                    case Triangle:
                        return "Triángulo";
                    case Square:
                        return "Cuadrado";
                    default:
                        return string.Empty;
                }
            }
        }
    }
}
