﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galicia
{
    public abstract class Language
    {
        protected const int Square = 1;
        protected const int Triangle = 2;
        protected const int Circle = 3;

        public abstract string perimeter { get; }
        public abstract string area { get; }
        public abstract string emptyList { get; }
        public abstract string reportName { get; }
        public abstract string shapes { get; }
        public abstract string total { get; }
        public abstract string Description(int type, int count);

    }
}
